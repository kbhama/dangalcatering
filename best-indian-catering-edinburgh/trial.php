<?php 

	require_once "../dbHelperPDO.php";
	$dbObject = new database;
	$con = $dbObject->getConnection();	
	
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Primary Meta Tags -->
    <title>Best Edinburgh Catering | Best Catering Indian Restuarant | Dangal</title>
    <meta name="title" content="Best Edinburgh Catering | Best Catering Indian Restuarant | Dangal">
    <meta name="description" content="Specialized in Indian traditional tandoori and curry dishes, Dangal has won the Takeaway of the Year award in 2020.">
	<meta property="robots" content="index, follow"/>
    <meta name="keywords" content="Catering edinburgh,edinburgh Catering,indian Catering,

edinburgh Indian Caterings,

edinburgh indian Catering,

indian Catering edinburgh,

best edinburgh Catering,

indian Caterings near to me,

indian Catering nearby me,

indian Caterings in near me,




best indian edinburgh,



about indian food,

takeaway indian,
indian Catering edinburgh near me
">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.dangal.co.uk/best-indian-restuarant-edinburgh/">
    <meta property="og:title" content="Best Edinburgh Catering | Best Catering Indian Restuarant | Dangal">
    <meta property="og:description" content="Our Indian Catering in Edinburgh serves healthy, Punjabi and Northern Indian cuisine. Indian Lounge have been dishing up delectable Punjabi food in the heart of Edinburgh for over 30 years. We use only the freshest ingredients to ensure our food  tastes superb. Order or Book a table today!">
    <meta property="og:image" content="https://www.dangal.co.uk/best-indian-restuarant-edinburgh/assets/halal.png">
<meta name="language" content="English"> 
		<meta name="copyright" content="Dangal"> 
		<meta name="distribution" content="global">
		<meta name="robots" content="index, follow">
		<meta name="abstract" content="Official Website of Dangal">
		<meta name="expires" content="never">
		<meta name="rating" content="general">
		<meta name="page-topic" content="Dangal" >
		<meta name="reply-to" content="info@dangal.co.uk" >
		<meta name="allow-search" content="yes" >    
		<meta name="googlebot" content="index, follow ">
		<meta name="bingbot" content=" index, follow ">	
		<meta name="HandheldFriendly" content="True" >
		<meta name="YahooSeeker" content="Index,Follow" >
		<meta name="geo.region" content="uk" >
		<meta name="State" content="midlothian" >
		<meta name="City" content="Edinburgh" >
		<meta name="address" content="215 St John's Rd, Edinburgh EH12 7UU">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://www.dangal.co.uk/best-indian-restuarant-edinburgh/">
    <meta property="twitter:title" content="Best Edinburgh Catering | Best Catering Indian Restuarant | Dangal">
    <meta property="twitter:description" content="Our Indian Catering in Edinburgh serves healthy, Punjabi and Northern Indian cuisine. Indian Lounge have been dishing up delectable Punjabi food in the heart of Edinburgh for over 30 years. We use only the freshest ingredients to ensure our food  tastes superb. Order or Book a table today!">
    <meta property="twitter:image" content="https://www.dangal.co.uk/best-indian-restuarant-edinburgh/assets/halal.png">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/odometer.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="shortcut icon" href="assets/images/favi.png" type="image/x-icon">

	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146467096-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-146467096-1');
			gtag('config', 'AW-360304181');	  
		</script>
		
		<!-- Event snippet for Catering Enquiry conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
		<script>
		function gtag_report_conversion(url) {
		  var callback = function () {
			if (typeof(url) != 'undefined') {
			  window.location = url;
			}
		  };
		  gtag('event', 'conversion', {
			  'send_to': 'AW-360304181/QogBCOjsme4CELWc56sB',
			  'event_callback': callback
		  });
		  return false;
		}
		</script>

</head>

<body>
    <!-- ==========Preloader========== -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- ==========Preloader========== -->

    <!-- ==========Overlay========== -->
    <div class="overlay"></div>
    <a href="#" class="scrollToTop">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- ==========Overlay========== -->
	
	
    <!-- ==========Header-Section========== -->
    <header class="header-section" style="background-color:#b11f28 !important">
        <div class="container">
            <div class="header-wrapper justify-content-center" >
                <div class="logo">
                    <a href="https://www.dangal.co.uk/index.php">
                        <img src="assets/images/logo/dangallogo.png"  alt="logo">
                    </a>
                </div>
                
              
            </div>
        </div>
    </header>
    <!-- ==========Header-Section========== -->

    <!-- ==========Banner-Section========== -->

	    
	<section class="banner-section" style="padding-top:110px !important;">
		<img class="img3" src="assets/images/banner/aimg3.png" alt="">
		
		<div class="container">
			
				<div class="row mb-5">
					<div class="col-xl-12">
						<div class="main-content">
							<h1 class="main-sub-title wow fadeInLeft" data-wow-delay=".2s">
							For your special day,
							Dangal Catering Services
							</h1>
							<h1 class="main-title wow fadeInRight" data-wow-delay=".2s">
								Always Delivering Amazing Experience
							</h1>
							<a href="#book" class="hero-btn  wow fadeInUp"  data-wow-delay=".2s">Enquire for Catering</a>
						</div>
					</div>
				</div>
			
				<div class="row justify-content-center">
					<div class="col-lg-12 col-md-12">
						<div class="text-center">
						<h3 style="color:white">We Provide Catering Service For</h3>
						<br>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
						<div class="img">
							<img src="https://www.dangal.co.uk/assets/images/gallery/6965birthday-party-catering-services.jpg" alt="">
								<a href="#book" class="text-center" style="padding-left:40%;padding-right:20%">Social Events</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
						<div class="img">
							<img src="https://www.dangal.co.uk/assets/images/gallery/4547583811252.jpg" alt="">
								<a href="#book" class="text-center" style="padding-left:35%;padding-right:20%">Corporate Events</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
						<div class="img">
							<img src="https://123weddingdirectory.com/wp-content/uploads/2019/06/Gourmet-Catering-Services.jpg" alt="">
								<a href="#book" class="text-center" style="padding-left:40%;padding-right:15%">Funerals</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
								<div class="img">
								<img src="https://www.dangal.co.uk/assets/images/gallery/4680624-AlastairGill-scaled.jpg" alt="">
								<a href="#book" class="text-center" style="padding-left:40%;padding-right:15%">Weddings</a>
							</div>
						</div>
					</div>
				</div>	

				<br><br><br><br>

				<div class="row justify-content-center">
					<div class="col-lg-12 col-md-12">
						<div class="text-center">
						<h3 style="color:white">We Provide Catering Service For</h3>
						<br>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
						<div class="img">
								<a href="#book" class="text-center" style="padding-left:40%;padding-right:20%">Social Events</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
						<div class="img">
								<a href="#book" class="text-center" style="padding-left:35%;padding-right:20%">Corporate Events</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
						<div class="img">
								<a href="#book" class="text-center" style="padding-left:40%;padding-right:15%">Funerals</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="service-box">
								<div class="img">
								<a href="#book" class="text-center" style="padding-left:40%;padding-right:15%">Weddings</a>
							</div>
						</div>
					</div>
				</div>	
				
			
				
			</div>
		</section>
		
	
	   <section class="menu-section" id="menu">
        <div class="container-fluid">
		
            <div class="row">
                <div class="col-lg-12">
                    <div class="menu-secrion-innner">
                        <div class="row">
                            <div class="col-lg-12">
								
								<div class="section-header">
									<!--<h6 class="sub-title extra-padding wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
										explore our Takeaway food menu
									</h6> -->
									<h2 class="title extra-padding wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
										Catering Menu Categories
									</h2>
								</div>




                                <div class="menu-tab-area">   
									<ul class="nav" id="pills-tab" role="tablist">
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#soups" role="tab" aria-controls="pills-tone">
												<span>Soups</span>
											</a>
										</li>
										
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#starters" role="tab" aria-controls="pills-tone">
												<span>Starters</span>
											</a>
										</li>
										
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#kidsmenu" role="tab" aria-controls="pills-tone">
												<span>Kids menu</span>
											</a>
										</li>

										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#grills_kebab_tandoor" role="tab" aria-controls="pills-tone" aria-selected="false">
												<span>Grills/Kebab/Tandoor</span>
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#main_currys" role="tab" aria-controls="pills-tone">
												<span>Main Currys</span>
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#biryani" role="tab" aria-controls="pills-tone">
												<span>Biryani</span>
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#breads_rice" role="tab" aria-controls="pills-tone">
												<span>Breads & Rice</span>
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link" id="pills-tone-tab" href="#salad_raita" role="tab" aria-controls="pills-tone">
												<span>Salad & Raita</span>
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link active show" id="pills-tone-tab" href="#dal" role="tab" aria-controls="pills-tone" aria-selected="true">
												<span>Dal</span>
											</a>
										</li>
										<li class="nav-item" role="presentation">
											<a class="nav-link active show" id="pills-tone-tab" href="#dessert" role="tab" aria-controls="pills-tone" aria-selected="true">
												<span>Dessert</span>
											</a>
										</li>
									</ul>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
			
        </div>
    </section>
	
	
	
    <!-- ==========Banner-Section========== -->

   <!-- ==========Services-Section========== -->
    <section class="service-section" id="service">
        <div class="container-fluid">
            <div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="content">
						<div class="section-header white-color">
							<h4 class="title wow fadeInUp">
								Catering Special Menu 
							</h4>
							<h6 class="sub-title wow fadeInUp">
								(100's of new dishes. Choose any dishes from our regular menu or from below dishes.)
							</h6>
						</div>
					</div>
				</div>
            </div>
			
            <div class="row justify-content-center">
				<div class="col-lg-3 col-md-6" id="soups">
					<div class="service-box">
						<div class="img">
							<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoGCBUVExcVFRUYGBcZGhwcGhoaGh0bIx0jISMgGiAfISEcHysjHBwoIRwcJDUlKCwuMjIyGiE3PDcxOysxMi4BCwsLDw4PHRERHDMoIygxMTEzOzk5NDExMTMzMzExLjEzMzExMTEuMTMxMTEzMTExMTExMTExMTExMTEzMTE5Mf/AABEIAKoBKQMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgcBAAj/xABBEAABAgMFBAgEBQQABgMBAAABAhEAAyEEEjFBUQVhcYEGEyKRobHB8AcyQtEUI1Lh8TNicoIlQ5KissI0g7MV/8QAGgEAAgMBAQAAAAAAAAAAAAAAAgMAAQQFBv/EAC0RAAICAgIBAwIFBAMAAAAAAAABAhEDIRIxQQQiURNhcYGhsfAFMsHRQpHh/9oADAMBAAIRAxEAPwBoi1dl3JOHLE5RXOtGWv2hem0Mmh7nzr9or/EOCVFkpDlWg5wqEHKXFD5NJWwm0Wpnw3ewfb74Y7FsjS+vmaOgHID6jv0jO7LmLtM+XLSkJlqULxLlRT8xNKDsjM8o13Si1JQkS7jhQIGAAbB3y+0aajhi5t3X7i1yySUEqszO2reJovE9gLAuOe1xYeHnCRaOsUUS5YFwYg5EslzmqoG+GNrSsVDMO02VHwfFn5NCeXb2Su4QCo3SnNWeGgYV3RxJZJZZOUts7OPCo1CKITUBroKlKfAVY7hmSNwgC2WQM9Tj/EPlpuykqQrtPoxDg1vcaEb90ATAOqSDdCnLmrk764b4rHPyiZ8Dg6kjN2efMkzBMlqKVJLuC0dh6J7eTapAmAC+C0xOhYMQNDd73Ecw2hZgACxAV9Rwdzg2IGusNPhZb+qtyZaiCia6FNgTke8Dvjr+nyc1xl+Rys2Pj7kdOC3zOZD7nPpEJs9uDZHdEE2mUtV0AoU/ZvUCscMh57qQMtSkm6aEEg13Hw+8NnBx7FRmpFsycxYF6ev7GIzLQQaO9HwPnAfXly7UGZzrrAk/aLEtdvZVJrVs+EJbDCbRtChY6vUZe8IFsd6bNEuWS5dRNCEjMn3UtAdsnIlJed8ynIQklyK46Cp0EaXoakmR1pTdVNJuinZQCQMNSFKPKM082vaaIYN+4uWi60qWK5ln571eUZra3Vypt4kqTQKKmPFt0atc03RdOqmOWIfeWHjCqfYBMZ0oN4l71ebERxZZOU97OmkorfSENrtgIdApUCgAANHI5wrsOxEzQTQOAz4E6eXfGltlklE9WgoXNRdAYgEFLNTSnnFE6xrQ6ZgS6i94DH7ikMU1j6vsz5IuU+tfsYO1KVJmXpaikBRusTRjkcY6b0H6Ti1IKZhHWoS/+YGKqfUMT3xlukGzQpMtILTFEgIYj/Y0phjp3xn7AZlitEtQUkkMoFBccOYpzjo452uS7/cVLHqn5O1TZuIfTxbdA61tUsXJfLXhBDyZiELTMSkzEpWlKjrXE90A7QQUfOC9SGNDzzxjXGcZdGRwa7LZs1gAaFt+DU98YFmT0vifmGurjLfCq2W4MAcQMMd2fAwPPnC4JkxV1ALBsVEZDWndDYRcnSAlJRWxja9ohIxD1H1aQXYwJUsT51VL/poIyxBI1OLZP3IejNq/FWpCEyyiUgFcwlQJUlNAktkVEA1NHhttm2mauYsUuJZGbu4U+gYvCvWZY+mhaabfXwFgg80qa0uwG1LmTl3lqLE0H0j3qYW22zALUHvJBbu4Qysy1GShIcteZ8LoDvxd++CJUpS0pUJMxSksQpKFEFi9WB4R5mU8k5tu2zspQjGqpCKbZUXCEJctW8W7m9YR7R2NfSVpDFOJGMaq3TCgkrSUKvEm8Gx4wBNlggKqdRkeb4wzFlnCV9AzxxkjPdH+kM+wWjsrvIdiPpWBjwO+O67It8u0SUTZZdCkvvBzB3hjHCekVnlrSTdUJobtYuRljQNujW/BDbHaXZ1KZKgZiXyUmihzTXlHp8GT60NvaRx8uPhI6ol/CPb3GIIIIcFxWoLgxMD0yi2q0xadnz++UWd/cY8CTXD20SvH2P3gSHCZlqoB683wHswVOWfwxwczU3sBRlK8wO6Fs6UQ5oGbPJ/HCLpNoCUlC3KFY7s3gsLSk0/KaCmm0Ovh3NBtadRLWctANNFGHXStPWTRLZ3SAkOBiSpRclh2EEcSIy3RX8m2SlXgUKJQ/wDmLofm0P8AprLPWS6ULpfIPgT3nvgfVY5L07X3D9NOP1r+wBNmIVIYpciWfzHPZUCpgxo6hdqNOcZZxLVLWgC8CbyS44VNO7SNFaU3UJTeopLbh+pRq7Vcawr/AA60dXMmSwZax2SKls/lqWjj45NKq/8ATuQS+onZC1Fwld5jgEJevFgxo9QIhOl9ZeEugS5IWwZq97vRtItRakFawElIZgqhw7WX1ECnExZOtEtU1V49WgpDgfWCxegIcdnfVTjCDhH5NnqMuK23sVmb+WoG7XI4/r7Ojs0CbAJFts5GPWow4wZarQ8tKcQDepUV1ybhqX3W9BbKZlvlqu9mU8xTf2h/MeMbfSJuaPP+pa4s3VoI6yYKNeNHbNvt7EF2+YVSZEw4m8hR1KCwP/cYXWaUuYsslVanR8a9/hwgzaqkhMqUkuJT1wdRqo+Qjozi0nfV6Oemm1QrUuqnbDQQFsZptplJIpervYk6RZbBXkfLnC6y2gypiJoNUF2zxr4GMWRNxZpg0pJsT7ctipkyYtQLqJ9WGGAFI61ZU3LPJQD/AMqUnvCQT4nvjmHSTZJKutki/KU6hdqUu7gjEgax0vZizNsklYLHq0cilkq5hld0Y/UNPHr4ZrwJqb5BMyUASAolIbtZ7w3FxA4Ui8opcf3ZjIUjy4oBQUbwAdziTVVcorK1lDpSH4Yxw+txR0ONLbssVsZEyamYlRl0A7KRUiiiTXGFfSSyrQsAzGerv2rtWwo2HpDTYW1CpfVrHaFQ5SNAMBiWwqS2Ue9ILGZoKizUSGq2bUNHw7mjWq435Rlxv3/Yyc6cZs5ky+wABfc48cRURnOkclIUWe87m82ObU4w4nT/AMMtSBLJSQ47SuyfXnpCHa0wTGLm8SzHIfeH4n7k/AWaFf4Nxsmc9js95iLq0sa4KLcu1DCzzSqVNlkkhMszZbubpSe0ATVqeMBWeSUWazy2c3HIZ2K1FY8LsFzpRlSJqlOFzUdWlFXY/Mo5hw/eI6CrivkxO+T+DJbRtZKgXZk5co86bzSlaJY+WXKQAP8AJIWTXUnwEBWiWXqDho/rwg20kWqUEBV2fLF1j2RMSMK5KD+6RuxJuLS71+Zjm6abGXwpLotcyl4XEOBrfPmE90HLFVXWUzlzQGAvhchaF2qTMSUqUhEwA5hJUktr86YOt8u6QktQeH3jjf1mMkoX1s6X9OafIY9EdnpnTlJLhCReUl8a0HfGokbQnTSU2dCEoRS8t67gE4RjtnbSmImJmS0AKa6WBuq4gZ4YRqdhT1WiVMSsGUsqKklJIxGWZDvTfGb0k1XGL3u/l/CsL1UXfJrWq+3zo+tQTaSbPa5SUrbsqHmknywLRgdsWdUlakpYKQq7xqzx0SVKnrKZc+X8ikqE1JGRdmxqKU7oxvTlaTOUsEVPlT0ieo/ti3d35VNomB7cfFeN/wAsylulXiorUA4NSKA8NHYvuMU/DNVzacsJLpM1QfUEEd2cFCVexZWLv9P3fDjE/hNY+st5mJHYQZi+QF0eJEdP+m3V/iZvVs6pbUdR+dLoA3WoyUk0vNkoO9Maw3WkZM1PGsLdrJvy+rTVUwXQNxZydwDnkNRDGYcUjIAekdCW4qzEuz5Br70/mJvFYAfD20etu8oWEcZtNjYFtTnq++v7wAqQSCDTQ975b/GNROk59mtHqRjw3eEDTrKLrj5qYDDD3yhY0yE1ChUEjMN3jn3R0KTOFusYIpOQ14YVGf8AiqvsRl7TZCEk4dn7aiB7HaJtmmCbLJdqgnsqDYEZinukMjNU4S6YLVNSj2hnMQqUoluysMxD3cQ1BvLhsoGUOwhd4pUh2ukM/wDiMMhwMaBNulWtIYiVNx6tTMSxFCMRwY4OIT22wTJZF5BrjdDgb3pTujm5fR5IbW18r/J0cfqoS70/hi60pLFi4vB2IAIGNCnHEAwKnq3V+WTecgGl0AqxOdG8Yttd4EgA00BL8NY+lbOnTi6JZALuSLqUn/Y0Z6RWPDklpJlzzQXbFU4BLBOf0B3bKucb7oZs02KQVEATptVP9KWon7xDo90elySJi2mTQKFqJ4anfDiZLJLnMUrwPr7eOthxrBG5dv8AQ5uWf1HS6Pps5ZDVydqelcDAk2UTkKZ8mzg+YkMGzbF8i33ipcv9OAfA7vffvipzlJ22CopdCa0SyfHz/iFNpsrAOx57zj3RpxLJNTkze/dYCtKAwAameOb4kbyYAsxhtcyUSUUBAo1DxBjW/DTb4WZllWyVEmZKGRf50+rb1Qp2ps4lONRp36axnZ1nmSlCYgkKSoFJS7gg09PGEZMSkn8joZJRr4OwzUlIfKuPrviqWsgOpXPCEvRrpOi1gIWUy7Q1Umgmf3J0NPl8wzHTpqgopWGHvlHnc+GWOVVo7OKayRtM8mWiWuaVi72Q4IOUVLnKJcVfAirvC6Z2FHqwA9NKR5abSAmpAqMDWlaRUoO07KjpU+yvpAsJBN2+d+e6kIuiOyzap4UtN2WjtTD6cThzh9KsU20qDC4jNZ0zbWNLYLEiWgS5YupTUlqqOp9I6Xo8baqjJnmou3+QbaNp4FEsJIF0Fg9BQVwhPtFJXVRUSSMTrXd5ZwcUEE1DON38RC0JcJA413AekdOMFHowyk2Y+3WbR2zw0fR84Q22zEG8ktm74Yc8PLdG7n2YAPXDjoIV2yxBTprU5bmGYO/vhiF0Z7YG35ki1SpqyVIQSletxXZUN9GPFIjf7d2eCoTJZBQpN4F3vA1BHIgd2sc62ns0l2GD+fnDnoL0nMlP4a0uZTm4rEy3x4ozbjwhXqsT9Tj4t+7w2MwZPoz5Ja8j2bLX1RSC3aOGIfOmURsaloAaYulQ6lUO6tOUOLUClHWSyJiClgtLEEODRnaurwmVNSXUQwJNNPDCPM5MWbE+Mk0zswyY8m+w+f0itKk3etoc2SD3gPGe2zOK0kkB60GeUW2icnqyLooSQsY8Du35QrRZ5i1NKQqYouAasX5Ze2jTixZc0l22JnOGJPSQPbrW0pMtKWUTRJZxSpO4HOkdI+Hmwfwtn7SVdZNYmrXU5A5uXc8RpAnQ7ob1ahOtHbmUZJqAcn4PQecbaX7rHosGNYI12338L7HIyz5v7ELOgJdgxzOJ7zjFz4xCuuVfCJJg5Sb2wEqJEecVvuMWEezEa6eMUWc7Qh0vSmFd+dPffHoJ1o9Kn15+2j0T2btZ7t/vCKVrFCM3+/o8LGEJsi8oApOhf9oHnWPQB8e4qw95RdOmhKvuf3gS124IYAigyHH3/EQhR+ASXCgMfKsG2STMlhhOUkNQKN4DPBVPbRVY9nWm0KIQ6U+LGvAQ2l9CgzzFueZ990EpuO0W4L/kU2dZU35iCWegTjdfIawdKsyj8yn572hdauh0sfKSDuYeULlWW1WYvKmFSR9CqjuPDGC+vJ6bK+nHwaqTLSCaB3GPBRzi5ScDTDdoNeMJ9hbfTNUULFyb+k0fgcw55PDq+zY6+QiXYLVFV4M51OWhbIxWq7maH+ft3xMkXTQ45cWy3+UQK6YEaF/enjFFFNplpBNSKmmOb5cPERDqwXPDHUPqN0XziSrMAuTUelc4pmJYmunnuEQgBag8th+kYt/GkJp1lJcgY8KVH3hxaZguuDhk+LAae/CFdun3XSS5fF+OTQDDE9o2MCcMMKNofeUOLDbbXLSElXWpdgJgvNh9Q7WesWWWyrmJ6yaSiUA7nFW8A0Cd58YotW0WZMiU7milVf8A6vQQrI8bVS2FjlNP2DiSi/8APISMqKI114QfZtnpSxElIo+D78yYwi9pW1RLTLrEPUgB9WiCelttkKIWRMSCQxAUO8MfGEww4b0hss+byzqok4OS4yZqRJetT/EZrov00k2khEz8qbgAT2VPkC1DuPjGoTjVw5OnDMNGuNVSM7k3tlCsg+YIcHc+ej98VGW7YYNh70EX36DGhzrSj4boHmFmrnUjjXyEWQotKCqgAAYVA1bfTCArTKZ2oacMiciMvbQ0KsBmN/s5xQUkqIAGOp1EQglXZXUb2DEim/Qjj3iF8/YaV0eg/tZ8K4e2eH9pUQSWGmLuHf0gRc8/KASVfKAHJJwAGZc+EQsE2Ts6fJrKmkJo4x7xgaagxobJKXNDTZUpRdsCDg70LeEE7I2FMCQZ6rpLG4gAmmtGGOT8oMmTLnZlyg4/UR6n0gnkSVSf+QavaB7PsKViZSBn7rDexWVKB2EhNA7Ab++EFq23Pln+kk8Lp9B3RPZ/TGUpVyYgoVuBp/qeORilnT9q/wBFOL7Zp0esSHD3WIWaahaQtCgtJwINMu47jFiSN3jBFH2vD7R6gk98eBe/KJV984hD4pEfXRHwEeOYohzFczs/Ljvf3h474pXMDcDT37zgQ2pkuWOTMKYn3x3wBareAL1QBUnwA3kk09iAGoLtdqo4Uzgh6U7zDLolshc9aVKB1D5DMn3nGY2femz5aSgBDup6lQSCsgtRLtkdY7H0TsnVyb2aj4D94lBJ8VYdIs6JSLqEsB4nU6wpsu0FKmJSpJ7ZLAD5QH7SjyHBxDsisVhDAsBerhFWABWlKRQkQl2lY0lymGsiYUpvAOFMVLxqzlxiGqN0U25ROYLh0nTjuMC0Gjn+3dnFwtJurTVKhkRDTYm2Otli92ZgN1QLUKW1yLgjcYN2lJd6RmrOpUqdNSlh10pQqHqntA8QkKg4vwSkzWGetiQWGJqNd0QVOoKvqDxOXfGblLmSmKiFSlN2kgi7oVJOW8Hk0OVLKWTgQONfUcsoMW410W2heI0BGI1+zQvtNqAA31qaeTYlm3R5OtKg5JGZFOf3hcF9YspdgA6izMBjwgWyuiE21KUSlIBpx4Za+UGdHdm9YtUyYB1UsaveOm4AY8tYzm1ttkAplC4gO1O0d5fDhjG62av8NZ5STdJQkLmP9RUHU+5zWmCRCsknGNkinJpAU+YJ89Ms3WBoguBT9TipwbIAiIWmyJSWKS63KCkhgahq4MWo2W+DbIuTOki0pm/mFV1S0D5XFUlJ8xGa25tNRtRlyV3iwK5hT8pJySTVscaxzpc5TfHXk6MFCEVY4l7JSgoEyZdel+69WBF4DJzQwom2VHWJBQV3lqFSxqP1Yfqq0aGxWdcuVcVMK1qSA5F0hSe05KnYYVwO54W7XQmZcWlCusBPW3mOdHu0NXwhkW10wZRXwc/2rYyFqUkFObFnY1DtQ0aOhfDbpIZyeoml5qA8sk1WAKpOqkioJxArhCTaFmABChXI1oNG0hBJMyy2hExNFIUlQrjmORqOcbI5LMc4UdmmTGwd/RhrFPWtdGGnf+4HKCLXtZCyFGUDLISoKQe0EqSFgtwOUC25N24oKBlqBUhQwIxrvcjw1hti1Kym0WjeQW3+cCTbSEmqi/HgNd8UWueczRsmz4e6RdsqyIWhdptCyiQjeQVHSnIMKmLSstuhZPnFThIUpQr2Reap30ja9Edifh5aZkwfnzAMa9Uk5D+7XfTCFHRXbKLZaxJkygiTKBmK1LEBIIAaqiCamgMbe2HAnWByyUI2uyRTk6PRJArniSanu9tSKLTIeqTjllxPKI7amrloUtAdQAujdmzRRsHa6ZyTTDElqHSMcprlxfkYnRYuyg1I3vn/ADwhJtzYMucHLhWR34PDufOZRBUQCKUp94gtYAF564ekNjKL0ySi+0c92bb59gn3FF0nX5VjforfiPPplhtCZqEzEF0Kch8siDvEY/pNY+vkKoBMQ6ktjT2RHvws2q96WqqVAqH+ScW4p/8AEQ3G6dePAuXybbI4YRJGf7R8AlQNw4YpzHrFaUirefGHgElKwbnHl6PkltY+v+6RRZw6Za92LZb3984ptyB1Us6zVvxCU3cNxUeURnIIJpTmPLlHipjgoU90kENUhQo9caOCNDC0NQX0S/8Ak/8A1r99zx2nZafyZf8Aj6mOHbEPV2iUskXCSkqSaALSZbly4AvPyjtnR2dfkJGaSx8/vBU6I+iyeslQQCxxJ0/eKrRO6sjEjBVSTXMd4iNonCXPBIPboOTmAtu2YiYFh+qUGW30kgpJ/wASk8iAYFInkV9MNozZRQZSklGN0UJzcEUNC7HGsEbEtyLTKNBeSwLZghwRuIPnCnpXY1K2eE/OqUsMvDs/qxwDkNoOD5rZNnmTVJSAoBQAUoOAUAlRJApeN4gHFyTkYGTpi3Jxno2NuQA4fDKMnbktOR/v/wDmuNFPsyJZN1LPph3YRnZstUyesIb8uUtZcszi55FXdFx2xyPbOq9LYil2tHcGhw5wRKmky5JW5V1Uu8WzAKdRW6Ewvs8kqFwBnxDuSH5hI5nSGM2U1Oy7MdzDybyhoLVLYBtG1MM89NIp2S6kWlIe8qSq7vYglv8AV49tlnZLMHqGG6mDQBLmrlKExDBSXq+5yD4wDAatCJYF9IOBIB0xGm6Ou7TlSlWqXLmOAu+AQWrQXeBBjm20bLKmvMlq6teJl0x3Z+kdFtSjOlSJqATfCVUAN0n5ic6doAawnO/YwsLuaK9tbNkSBdkukE/mJCiUk0AUxND9oxsxJTPWxAcFJUkkuKsWTUpB5so44HaT5d4zQqZ+WFC87BRISDSmp5tC5dklzD1Uy4U3bwJFQXwwLKYVbG9GCE3y938+xtyRUoe17F9m2xMlzAZpvS9aKzxDCpBY0rupFlplIST2jcUm84IAUSWSQQSFJfTQ74lM2IlVwGazuQSSpgGNNHfEb4gNnS5c5KFLKkrulMxJ7BJ+ZJU12+WBY7oYpRr2iUs1+6ga3TkEi690OO0z41cjHOMr0hXeWku+Aja7fsoTNAvuFJAF7EMLtd9Mc4xu1rOozxLbtAsRvdvOHYq5F5f7bN9YJv5FnBJDyZfgG8gIPkEGyzUnCXNQUbrye0K6kxTN2YsKTLCFPLShAeg7KUpJwwJBPPjEtpDq5KZKFEkqvTFDMswHDKNSMndGf2jPYsNBk2Dae8NIK+IU4osdglp+RUtUwgNVXZD8rx/6oT7RQyicix8zDmypl26yiyLUJdokkqkqVQEHFB3YYYMkwcd2i5fIX8BUgqti82lJ8Vn0HdHTlIvODh6+/OOZfB2xTrLarRIny1IMyWCl2IUZaj8pFD2VPyjp1rUyVa0bvhHqFStlwe9CzaSVpBKbrMezXGrNuOcZuxyUS5a1lbmYCZt3BAeksAYremvfG3tKXcA++GsZO2bC7YSZ7S3VMJORLO7MDiWfBzGHLGtJWVNSkrR9s3aUtCSS5WRRxTuYHGG02ekovA3qYhqwhsuzzMSSEpuhwLxPaD/NU40dt40hjYZKZaVALSkYkBLbsCk98Fhk1Hj+pMal34F1qNwpON1KnOpLDuF4ne0Z/oGq7bABh1rCjUU6cMqGNFtRYJKkqHZ7TihpTDhTBi8KugckzLSqY1L65lNA7f8AcRD8KfLXQyfRvLQSBfB7SASN7YjgRBU3EnVj3iBjJv8AZTnQnQPXnlzgieoElsBQco3eBHk8vRFo9OGvsR9eiizjS7ODiKto/P3pFM2xU5PQHfuhz1LAlub+8vOILs5ABbJiDzGunlCqGoy9rsibuDc/tX2Y23QHpLdVdmFz8q9+V714vqIz1sstC790AzLKpKnS4INCOP2i1YSa6Z3S0yUTUPQpNQR5iBES5iaOkjUk+Tesc+6OdL5klkzR2c3w44091MbCx9KrNMD37p0NRyiMri11tBG2JV6SqWfrdPF6Pw9BFK5SZcpKE4ANh3mPbTtyzgOZqYzO0OkqVKKZQVNVkwYd+kU0Uo/Yh0jtglpKieAzOgEKNl7PKhemUUolShVtAnkPOD7LYFzF9bON5QwAwTw374b9X2QxalN5qdWxG6CjGi3KugCVZAhgEgB8m1b33ZRG45AY5acYYTkuznNstcvCKloqMTzG+DAE9qlk0ag3Bsv2hPa7LiGYYmg5jyjUKkYkijfaBZ9i+YM7fsM98CyGCtkmtHHL998bH4ZbaDGxTSxcqkk8XUiveOKoC2hYL2YDgYEae8oQWqxKCgQSFCoahGBcb84GUVJUyebR0idLVLK0KSFBRN1WaXLsc38DSA0BQJvKuKSLySRdIFBUn/I1gPYPSxMxKZdr7EwUE1uyr/NvlO/DhBW0tnKN9YJWlQSEsbwzev6S4LMc9Y5+TBKLtdGzH6iLVS7BJk1F9DKSUrlvdSWxLgcaYZQBO2NMmi5fVLSCVpHaF5+KWvBznnBVumKJlNLSky03WqkFswAKUBAP92sQ2btS0JFxEu+byqlwCDgQGJfdugIY5JtxDnljVMS7ZvyilK1KPZ7KiXphyaobUQy6F7OUZhtc0E3f6YVUqKQw43QHOpaD7J0aVNmCZaVEs5CRiauXaicePCNKqzgBgkAAMEjIDINhG3FCtsyZJ89LojOtMxd4qUd4Y7m89YX2iXeZzUF60pDMS8SDT+IotCFOxLc8cYfQtJIzlusQ40fHzA4nvjP7RsikG8kkEMyga7m8O6NxNlOn6tcSd+nswpt1ifEOePD3yiFiaydMrRLmSlzPzDKW4LMSMFJ0N5LiO52O2S50pE2Wq9LWkKBGh9cQRHA9pbNfAHPzO6G/QDpNMsKuqmJUuzKLkCplk/UndqnnxkveqYFU7R2sC9UV3wBOShSihYBBoAY+2fapcyUJkiYlaCT2k1GrEZEaEPH0+Wom+lQw7VBGWcGu1+I2LXySmHqwAlIujJIHrjC+3LSdSd2PhQd7RbblqAvYhILuQxwwwruhTPtEyYAJQocOzdbux5Y7oDblxC0kLNvJZHVgfmzMToHxLeEaHobYRZ5TqT21gMNE4gcTj3RXsnY4Qb80310oa8H3D9PfDkB2J315GNeLHw7EzfIuVOKqYDdELp8o8Co8ypoPKGgkyYleiCTnlFrbjEIczs0rhv8AD3zMTCA+OZwBOoj0rYCr/wCvD28fAhhXx4798ANB1yXbSr4DziiZIS9Rn+qJWiaGZxXPfFezrNMtC7ktOQKlF2TV3J5YZtuiIo8mWZGQTmWf78fHfFtl2QhfyyirNwk6age+Ua7ZXRtCA4AmLGMxYo+d1NW8TvhkrYoWAVTFKG4tugnSB5GMs/R1NVGSrCjg6b98Gy7KAQyWOlBkAWh3aejkv6SsHULIhfNRaZVQeuQPomte5KFYq0S2yciWxAIOIHjhpjESgXgHP6uPjWh8IlZDKngrlOlafnlKopP3G/yeISgzPilLMa7jzekEUSWhjQVNc9XweBFAEkDQ/YYq5RfQHKmHfEDSgZ9/ExAigocd+Pv3yipUn5izfupqMd3jFiVAAUBxOX2pr/ERnzQyWYPTEavxz8YhBdNsod2pjri+tdfYgCfZgSzDHMcPTzhpaZ7AdmmZFcuG+FNu2gAxAq5x5aDfAFlQ2cl7rZMDTfljlBNgs/Vf05i0VwSSAXY1qxo+UCzZxlovzTdo4QPmIxq+HDGE83bc1SQqWOrQSQOz2iAHJfF6MzxUpJdjJQUVc3X7m8lgkA9ZfxFUo14cucMLPIcGvkMt3COWW+3TUlJE+al8X8GAOG+L7L0qtchQdZmI0mJx4YHuOcBGcH4KUYNXF/odXShqAH2RvpEClTUZ+IBrz9vCTot0tlWohD3Jv6Cfm1unPOhrxh+lTsRizaEM/vuhoMouPZRODgjtMKeIr4xUpBFWPPhv94QVOwUCDQg+vrFQUas7VfDfqcfesQoFno7IINGz5Nxq8DmzgqDVBx8uGkGzQMXOGoOm6KZxFLrvvb374xRBZMst7IM+Lvrl3d8Ap2cK0emm7DAw4WtIPN6gbzru8YVTbUqZM6uUkrXuYBOFVFywFYptJWy0rdIs2ZZVSFXpcxUotUpeubEOx4NGl2ftyYWEzq10qSm6fAt4Ri9rbVlyWKr1om1q5TKDULZrZ2gaTtXaMyWZklKESxlJSh2GL1vU3Awv6yfSsb9Cv7mdRkzwr/loHf8AeCETKZAHSn75xxud0ot8tAWmffxvAgEUD0caA90N+jvxRchNplhsL8uhH+pLHlBRyWrrQMsSTpM6oTr5tE0ZY467oD2bbJc6WJkpYWg4KFeRzBGhgwEPQa+sNTvaEtVpnyA2J9ImBSjYekVg74kFBvehiyieETcxTngMYtfd4CIUctMyjscDWv34RQZzEEvicz691IFnT2AJLkirke9IO2TIl9Wq0Tqy0lkJxvKp3h/V4BK2MbpAciyzpt0S0k3lMCXAfDnjHS9k7KRZ5IlIxxWvNRzPoNBGb+H1rXarRMmKATLlIFxIFAVUBfNkhXfDvpXthMlPVsSuY4DFmBcO7H3WLnKMI8rFtvyB2fpDOmqmyZCUfl0vVIBJIFcG7KnJ5Aw16MWxQR1awSpGaWVQ4E3TicXGuEZroVsIrsiwlRlm8AGdIUwSVApJdSeP6jG02FsxMhDBRUpRdSjqa0zbIAk4Rjx85yUvGxat7C5M0KBaBLdKB379IPFdHiiZUeca/AxGB6RWWZKmifKVdWk94zBGY4w8s6BapKZ8pIcghSXwUMU4h64PkRF230BSCmhDZ8v5hD0RtZlTbTIQ4eWJqQdUsFNvIfuEXB06Ce1ZdPQUkpWCk41BGlYpXMAckjHIE8N8M7LtdM4mVPTR2CwGY4cjhxhLtazqlTFoLuMCAWIFQR/qR3QclREyCZwxxp78YC2hPGoo+VcPfsiKJ06mOAOPf6QsVPvKIJolyogPQd/t4AKMXJ0gi2Wm8GQ5fNuIxOES2NZgE9dMIqq7KBAqokJfeXw5whtO0JkxaZcs9WFKShIADuTdBL+Ube0J6oXSgmWECXLUkXj1rpUgMHIyL7zCM8+EG134NuHHBPkndf8ARi+lFknJmIKgQqYshr2aSQwGT1O+BJC03klSyySEMa6VPa7JoQ7Ab8I3Vg2Z+KVMVbFJBSpTJYouk9p2NSzsOEL7HseyItkyRNCVBclLKWQe2XUpn+U3SktuMc+PqItNS7QrP6eWWViSbZ3UtSJapho4uhIFGJBIcUAPLGK1ylIlsAk3iakXlpG4HAUjV7M2lLRIMsJDy7yOyPmuEovUxSQARAu2LISZc0EFCgAFJfe4bT7Qr675U10PwejjCm3b/Q59Os5SQZfzCtKYHF3y1jqHQjbv4qUUzW66UBfwF9Jpf/ydgd7H6oxe2yHCwGo1Bl+9YG6KbTVIt0qYQybwSsapV2VA8i/IR08GRyqyssFdHXpiS2Acgabt/GKitnoA/A6iIW3aEvrVpmI6sAt1iSCkMfqDukYVp5RRbE9Wbp0dJdwQxII1GEaWZJQcd+CdoUAnCrF/AuwOFIXrtVyrhxSr589x8YDt1qLY5EY61fz7oHtlsRJlJmzXUpf9OXqHopXfvgJy4khHl+BTtCdMWoCWHKlMFZVoAHxLnl5MJVm6paLMhKw5vTJgSGmEBykEqDISSncavvA6LW+baVTZxA/KS0pKRgtbgY5hIUY+VapkuQiWoqBJU6lEKUlLtdZ6Oz74weoyy6aNmHHFbTBJ6JM2fMM6atNSxLF+aUkcABzgyzIs6pUzqwEG/dlqvLulSS4VTGgGVH4Q36HbJRPlz5ajflru9W5wxcsCQHJx3QHsfZhkTJktV5ZQu7LSok3icGGDNj44RFNRhdX/ALJ3Jx+BeixgzCJ6Zagsk3pZdyACUkA3jR3fGMTtgpWtXVyUoAUfkKj2RR1ByMauGZiKx1zplsiXKsaLoI6pRN5LghwS+Pyv2QHwUIwsnaUxIKpckCWmsxQQNC+AdnL8shGnFkrXhiJpSVrwLegvSiZYpwIJVLU3WS3oobtFDIx3qx2tE2WiZLIUhaXSRmD65GPz1tqZLnEzEIuLBJUwZxVuyBi1Sqn26N8HNtPJmyFC9cZaA+RLKD6Ox5w3kovXTFuPNb7R0gJz0j408KtAsielRYFSVs5QrFtU5KHCL0rEMjJS6EuLi9lxx97o8vbz4x9eBPjEer/t8oIo4OqYpQNDnGi26j/htmu4XlPxb91RmkyiDh79+8Y0GxLdLMlVlnf01F0K/SrHlX1gYPtBSXk0nwZDSbQ+N9HddP7xv1WVCkuoA00jn/wzkKkWibKUoKTNQCgjMoJ8bqj3R0iRhwg2tKxbewSxbOCFXqGlHFQcHfhugialyA/KLVLYPESGdscYXGMYqkRHykjBoEnIZ9CXgtED2yUVApdgc4jLQl2klyKYCrRm7AB//QSoZSpyVcLhPm0abapYu4YBiW96wj6PWMzpk+YFMBLMtKjheVU9wHjFRXuQT6ECbR2yA7kjI4vDjpwodchz2hKl3qHVWm6L7LsiVZPzZ0zrFiqUgZ7gSScM6DujPbZtBmzVzFHtFsMAAWAD6CkNlpFLbEu0J+QfxgeUHs9oP1Dqidbt4A78SO+LbbIq9ffOBU2rqlXmvJIUlaSHCkmhB5QA6DSewToqi9bpAJ/5iT3OfMCN9stZ/EzVJJN2YLyf0koTdUBmm7Tj4YeXLlyp0q0yl3paJiFlJ+YAEKI30cZRubdJItt1PyzEiYFZG4yFCm4oO8KMYf6hG8LfwbvSxp8ZBm0LaRNW5vJWlKhdIJCk3Qx0CvvCa22dEwCZLFybfUU9l2JcMcmq5G6COks2fLlqCZaLoYkhSiSlwCwCQHbfjFUm1IWkICmJe7uIyIxTwaOKrVSX815N8McHH8AeXNUtShOSlMz5SpCLgWBQE3SQTk8DzZtxPVhalIDsKsl9+L1bnGhVZEApCAVG4TUhgXoOYcwmtdnUopSlAUCpKVKdj2iz6kD0i4z5S/mgnKCgtdCjayJa0OCbyWA/uLPnlRu+MntFf5iavQ7mof27o0u2UFajLS3YWbpFGbMvoz82hWqwdZapcu7VRAJFcS2W5zHV9LrRgypSevlHRLXOJWsNmkmgNTdUctSY8E4/hSHI6qcUI/xUm+E1yBYDhFFoQtUxV1DuonQY5k4Bo+tvYlCWO12itZAoVHvYVLR0jJxaUm+vAj2hPBDOMNfeULviM/4kj6QiXdH9pSD5lUWT0F+Rbyi+2plWqUiVMX1c6WLsuYRRacQhWbjXzhcnTTYuPui4oY/DJRTY5hSQCqeEknIXRXiHPDGGvSZUuUpPVBKkzJagv6gGLA0zqYU9ALBMly7TZVll9icgg0IHZWxzDFI5mGk2zzZwXLmoJmSyGSlkhsiC7MQ45Rhzr3Wl+Zswq40/BZ8MrcJdoXKUEi+kFJDB2ypR2PgY2u1pcqWo2kpF9IYqarGnjQezHJbJ1aJ5vrXKKXKCA5Cx8oIbA4b+cP8AbO3pq7NLEwdUtVSv5kqZxQJcjI1NCnCAUmo1WwJY/fyfQs+IO2p024DdTKdTJSpySKEqoKVoA43mMWbSoOkEgKxD47juhmiQlZSDM7RLGhLDUcq5Qstku6ohwQPGGwa6KcHFaBrba3SEhKQBioBlHGhOlcNwjW/BdZFrA1lzAe54xdtAo2MdD+DNhImrmtREsjmph5RpbSivxQtW2/wOi7bV+X1gxlm8k7wWbmKc4ahe7EP4CFlolGZdQQyXCiM1MXwxCXDl8WhghVSPeH7QyDuTaFTVRSZaXJ3H+YjcOgiRG6PYaJONmxFmIJPPRudWgaZZaHjhXUfc90aD6RxHkYDmj184RY4W2HaU2QtExJ+RQUAXI4agHCmsdr2JtOXaJKJ0s9lQqM0nMHeI4Va/t6Rtfg+s9bPS5a4ks9HYVbWG43fYvIvJ1AkEcYgslO8ZesVy/q4iJWrCI9C6PJ04AOTSkVTZ4+YMRFNt+RfvSFKlF1VygW9hJFO0ZhMwJSAoqwS9M3J3DN9IWWy2KlgSpCyEgFykfMo4ndubSCR/RnqzcB82fB9ITWYV9/pg10SigE3lEklT/UX4VPPuiE5LuKCo8H3wRL+rj/7CIqwV71igxVMkvwaFttstDv3j3hD1qq4feAZvzDn5RRDI2yzlJoRhkeOnAxtuiG0DabMlAUBabKQUOfmSKJfVLHqzp2TGct/zK96RR0IWRbpDEh5qgWLOGw4RUoqVxfQ3DNqSOmbNtS58hU9SFBQKkmSwd0ukg7yag6EQrmypK5apga+tilwzYAEZ0xfdGmsJYLalD5Qq2ikdTKp/yx5R531EOEmonUxyfQq2ZZ1KvIM9IWn6CarTiFakO4ppWAbZbFfmVAN0Xc/lqCCPONd0ysqDZZhKElkhuyKYYaRmtrZ/4eggaSaYzHLmnZkNp7VSLyQk9ar5WGZz3w66DbMWj881U5uknOrqD6AMIyey62kvWudY6tYki6in0j/2jvYcUYrRz5Te5fBG0z1rJK1uaYfxAtsTewoQasBg7A6VxhjZ0h8NP/WKLV6DzRDzG8jk9mfttkFAGw076c4z+09nEjDMAHTw4RtrQP8AxHpGd2xEKEmxNuTbLaJc0krSg1BxKTRQfg+OYEdL2rb1oAtMhSZiJolhKiwF12yq7liGJxNMI5Rb8+PrHQ/hSXsU4GoTO7INbtAaaVrGb1GNVZowTdhM6fKmrJnyikmjAJL0xKvmBBpqIU7TsMszUSTPWpCkqVKdSQE1YpqKnupBnSg/8TXy/wDAQutaAuzWq8Aq6sXXD3cMHwjJjk3l4S+DXk1iU/IntWyFIUg3x1SlBPWil19QWbPNt8LLYhKVqAJWxN0swUB9RrQZxLZay5Q5uuOy9M8sII23/QRvmh99M9Y0JLlRn5OrFdhs8yasIAvdpwwqSacWpHbuimyfw0hMsFlEOspzUcn0ApGK+F6B1qqCiaUwrlpHS5PzJ4/eGQ98t+AcnsjonIlN7498ES0s/rEUR79jGviktGPk32TJw5ecfXvdIifX1j6IQ//Z" alt="">
						</div>
						<div class="content">
							<h4 class="title">
								Soups
							</h4>
							<p class="text">
							   <ul>
							   <li>Sweet & sour ( veg/ non -veg)</li>
								<li>Sweetcorn soup (veg/ non veg)</li>
								<li>Minestrone soup</li>
								<li>Mushroom soup</li>
								<li>Tomato soup</li>
								<li>Vegetable soup</li>
								<li>Tomato shorba/ veg shorba</li>
							   </ul>
								 </p>
							<a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
						</div>
					</div>
				</div>
				
				
				<div class="col-lg-3 col-md-6" id="starters">
					<div class="service-box">
						<div class="img">
							<img src="https://www.dangal.co.uk/assets/images/menu/6060chilli-garlic-squad-compressor.jpg" alt="">
						</div>
						<div class="content">
						   <h4 class="title">
								Starters (Veg)
							</h4>
							<p class="text">
							   <ul>
							   <li>Paneer pakora</li>
								<li>Veg pakora</li>
								<li>Assorted pakora<br>(aloo,broccoli,cauliflower,onion chilli)</li>
								<li>Hara bara kebab</li>
								<li>Vegetable kebab</li>
								<li>Potato fritters</li>
								<li>Broccoli Bhaji</li>
								<li>Crispy palak</li>
								<li>Chilli pakora</li>
								<li>Aloo bonda</li>
								<li>Veg samosa</li>
								<li>Aloo tiki</li>
							   </ul>
								 </p>
							<a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
						 </div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6">
					<div class="service-box">
						<div class="img">
							<img src="https://www.dangal.co.uk/assets/images/menu/1668fish-pakora-compressor.jpg" alt="">
						</div>
						<div class="content">
							<h4 class="title">
								Starters (Non-Veg)
							</h4>
							<p class="text">
							   <ul>
							   <li>Meat balls</li>
								<li>Chicken pakora</li>
								<li>Spicy chicken wings</li>
								<li>Chicken drumsticks</li>
								<li>Chicken salt & pepper</li>
								<li>Chilli garlic chicken (Indo Chinese)</li>
								<li>Chicken Honey & chilli</li>
								<li>Shami Kebab</li>
								<li>Chicken Monaco</li>
							   </ul>
								 </p>
							<a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
					 </div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6" id="kidsmenu">
					<div class="service-box">
						<div class="img">
							<img src="https://activeforlife.com/content/uploads/2019/01/children-playing-at-home.jpg" alt="">
						</div>
						<div class="content">
							<h4 class="title">
							   Kids menu
							</h4>
							<p class="text">
							   <ul>
							   <li>Chips</li>
								<li>Vegetable creamy</li>
								<li>Penne pasta</li>
								<li>Pasta with cheese</li>
								<li>Macaroni cheese</li>
								<li>Macaroni pasta</li>
								<li>Penne masala pasta</li>
								<li>Hakka noodles</li>
								<li>Honey chicken</li>
								<li>Fish Fingers</li>
								<li>Chicken nuggets</li>
								<li>Potato fritters</li>
								<li>Paneer fritters</li>
							   </ul>
								 </p>
							<a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
					 </div>
					</div>
				</div>
			</div>
			
             <div class="row justify-content-center mt-2">
                <div class="col-lg-3 col-md-6" id="grills_kebab_tandoor">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://www.dangal.co.uk/assets/images/menu/8688paneer-tikka-compressor.jpg" alt="">
                        </div>
                        <div class="content">
                            <h4 class="title">
                                Grills/Kebab/Tandoor (Veg)
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Paneer tikka achari</li>
                                <li>Vegetable seekh</li>
                                <li>Soya kebab</li>
                                <li>Hari Bhari seekh</li>
                                <li>Galoti seekh kebab</li>
                                <li>Tandoori aloo</li>
                               </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://www.dangal.co.uk/assets/images/menu/5123tandoori-chicken-compressor.jpg" alt="">
                        </div>
                        <div class="content">
                           <h4 class="title">
                                Grills/Kebab/Tandoor (Non-veg)
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Chicken tikka</li>
                                    <li>Chicken shashlik</li>
                                    <li>Chicken haryali tikka</li>
                                    <li>Chicken tikka pesto</li>
                                    <li>Chicken tikka Lal mirchi</li>
                                    <li>Chicken tikka three pepper</li>
                                    <li>Lahguni chicken tikka</li>
                                    <li>Angara chicken tikka</li>
                                    <li>Afghani chicken tikka</li>
                                    <li>Achari chicken tikka</li>
                                    <li>Chicken tandoori on the bone</li>
                                    <li>Tandoori Afghani chicken</li>
                                    <li>Stuffed drumstick</li>
                                    <li>Kalmi kebab</li>
                                    <li>Chicken wings</li>
                                    <li>Tandoori chicken pota</li>
                               </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                         </div>
                    </div>
                </div>
				
				
                 <div class="col-lg-3 col-md-6" id="main_currys">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://flipdish.imgix.net/N849oKDEqmoPF15QAXMfWHke7rE.jpg?w=120&h=120&dpr=2" alt="">
                        </div>
                        <div class="content">
                           <h4 class="title">
                               Main Currys (Veg)
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Seasonal vegetable</li>
                                <li>Gobi matar</li>
                                <li>Achari aubergine</li>
                                <li>Aubergine matar</li>
                                <li>Achari aloo</li>
                                <li>Jeera aloo</li>
                                <li>Dum aloo</li>
                                <li>Paneer pasanda</li>
                                <li>Paneer kolhapuri</li>
                                <li>Paneer Mughlai</li>
                                <li>Paneer jalfrezi</li>
                                <li>Saag paneer</li>
                                <li>Sambhar</li>
                                <li>Vada</li>
                                <li>Veg Manchuria</li>
                                <li>Gobi Manchuria</li>
                                <li>Thai red curry ( veg/ non-veg)</li>
                                <li>Thai green curry ( veg/non- veg)</li>
                                <li>Chicken hot garlic sauce</li>
                                <li>Hoison sauce ( veg/ non- veg)</li>
                                                            </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                         </div>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://www.indianhealthyrecipes.com/wp-content/uploads/2017/05/chicken-masala-recipe-1-480x270.jpg" alt="">
                        </div>
                        <div class="content">
                            <h4 class="title">
                               Main Currys (NonVeg)
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Chicken mughlai</li>
                                <li>Butter chicken</li>
                                <li>Highway chicken</li>
                                <li>Achari chicken</li>
                                <li>Methi chicken</li>
                                <li>Chicken Kashmiri kofta</li>
                                <li>Chicken Hyderabadi</li>
                                <li>Chicken dahiwala</li>
                                <li>Kerala chicken curry</li>
                                <li>Lahori chicken curry</li>
                                <li>Punjabi chicken karahi</li>
                                <li>Lamb roganjosh</li>
                                <li>Kashmiri korma</li>
                                <li>Kashmiri kofta curry</li>
                                <li>Lahori lamb curry</li>
                                <li>Lamb methi wala</li>
                                <li>Lamb Punjabi masala</li>
                                <li>Lamb Achari</li>
                                <li>Lamb sultana</li>
                               </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                     </div>
                    </div>
                </div>                
            </div>
			
             <div class="row justify-content-center mt-2">  
                
                <div class="col-lg-3 col-md-6" id="biryani">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://www.dangal.co.uk/assets/images/menu/2008biryani-compressor.jpg" alt="">
                        </div>
                        <div class="content">
                            <h4 class="title">
                                Biryani
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Shahi Biryani</li>
                                <li>Dum Biryani</li>
                                <li>Lucknowi Biryani</li>
                                <li>Hyderabadi Biryani</li>
                                <li>Fruit Dum Biryani</li>
                                <li>Jackfruit Biryani (seasonal)</li>
                                <li>Apricot & jimikand (yam) Biryani</li>
                               </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                        </div>
                    </div>
                </div>				

				
                <div class="col-lg-2 col-md-6" id="breads_rice">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://flipdish.imgix.net/JtUWrDBqGZX7z4XiY1VTSBHux4E.jpg?w=120&h=120&dpr=2" alt="">
                        </div>
                        <div class="content">
                            <h4 class="title">
                                Breads & Rice
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Naan</li>
                    <li>Roti</li>
                    <li>Missi roti</li>
                    <li>Bajre ki roti</li>
                    <li>Garlic naan</li>
                    <li>Peshwari naan</li>
                    <li>Butter naan</li>
                    <li>Chilli naan</li>
                    <li>Basmati rice</li>
                    <li>Jeera rice</li>
                    <li>Lemon rice</li>
                    <li>Coconut rice</li>
                    <li>Garlic rice</li>
                    <li>Curd rice</li>
                    <li>Pilau rice</li>
                    <li>Vegetable pilau rice</li>
                    <li>Kashmiri pilau rice</li>
                               </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                        </div>
                    </div>
                </div>

          

                <div class="col-lg-2 col-md-6" id="salad_raita">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://flipdish.imgix.net/vVw6Eg8fMzn5Lr8lMBoKFeSP7P4.jpg?w=120&h=120&dpr=2" alt="">
                        </div>
                        <div class="content">
                           <h4 class="title">
                               Salad & Raita
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Pineapple raita</li>
                                <li>Banana & pomegranate raita</li>
                                <li>Green salad platter</li>
								</ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                         </div>
                    </div>
                </div>	

			
                <div class="col-lg-2 col-md-6" id="dal">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://flipdish.imgix.net/2y7rAjCiSpfD56UwuMJ4rZ7bs.jpg?w=120&h=120&dpr=2" alt="">
                        </div>
                        <div class="content">
                           <h4 class="title">
                               Dal
                            </h4>
                            <p class="text">
                               <ul>
                               <li>
                               Makhani</li>
                            <li>Channa Dal</li>
                            <li>Dhaba Dal</li>
                            <li>Moong Dal</li>
                            <li>Panchrangi Dal</li>
                            <li>Rajma</li>
                            <li>Pinole channa</li>
                            <li>Kala channa</li>
                            <li>Karahi daal</li>
                                                            </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                         </div>
                    </div>
                </div>
				
				
                <div class="col-lg-3 col-md-6" id="dessert">
                    <div class="service-box">
                        <div class="img">
                            <img src="https://www.dangal.co.uk/assets/images/menu/4462chocolate-compressor.jpg" alt="">
                        </div>
                        <div class="content">
                            <h4 class="title">
                                Dessert
                            </h4>
                            <p class="text">
                               <ul>
                               <li>Carrot halwa</li>
                                    <li>Coconut pudding</li>
                                    <li>Gulab jamun</li>
                                    <li>Rasgulla</li>
                                    <li>Rabdi</li>
                                    <li>Jaggery lemon water</li>
                                    <li>Rooh afza</li>
                                    <li>Nimbu pani</li>
                                    <li>Jal jeera</li>
                                    <li>Thandai</li>
                               </ul>
                                 </p>
                            <a href="#book">Enquire Now <img src="assets/images/arrow.png" alt=""> </a>
                        </div>
                    </div>
                </div>
				</div>

            </div>
        </div>
    </section>
    <!-- ==========Event-Section========== -->

    <!-- ==========About-Section========== -->
    <!-- ==========About-Section========== -->

    <!-- ==========Menu-Section========== -->
    <section class="menu-section" id="menu">
        <div class="container-fluid">
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="text-center">
                <h3 >Our Packages</h3>
                <br>
                </div>
                <div class="col-lg-12">
                    <div class="menu-secrion-innner">
					
                        <div class="row">                            
                            <div class="col-lg-3">
                                <div class="opening-time  align-items-center">
                                    <h3 class="text-center" style="color:#ffffff;padding-bottom:8px">£15 Per Person (Veg)</h3>
                                    <ul class="time-list">

										<li>Pappad/Chutney</li>                                      
                                        <li>Any Two Starters</li>                                       
                                        <li>Any Two Main Courses (Curries)</li>
                                        <li>Accompaniments <br>(Roti / Naan)</li>                                       
                                        <li>One Dessert</li>
                                        <li>Salad</li>  
                                        
                                    </ul>
                                    <div class="button-top text-center">
										<a href="#book" class="custom-button" style="background-color:#ffffff !important;color:#B11F28 !important;">Enquire of Catering</a>
									</div>                                    
                                </div>
                            </div>
							<div class="col-lg-3">
                                <div class="opening-time  d-flex align-items-center">
                                    <h3 class="text-center" style="color:#ffffff;padding-bottom:8px">£18 Per Person (Non-Veg)</h3>
                                    <ul class="time-list">
                                        
										<li>Pappad/Chutney</li>                                      
                                        <li>Any Two Starters</li>                                       
                                        <li>Any Two Main Courses (Curries)</li>
                                        <li>Accompaniments <br>(Roti / Naan)</li>                                       
                                        <li>One Dessert</li>
                                        <li>Salad</li>  
                                        
                                        
                                    </ul>
                                     <div class="button-top text-center">
										<a href="#book" class="custom-button" style="background-color:#ffffff !important;color:#B11F28 !important;">Enquire of Catering</a>
									</div>
                                </div>
                            </div>

							<div class="col-lg-3">
                                <div class="opening-time  d-flex align-items-center">
                                    <h3 class="text-center" style="color:#ffffff;padding-bottom:8px">£18 Per Person (Non-Veg)</h3>
                                    <ul class="time-list">
                                        
										<li>Pappad/Chutney</li>                                      
                                        <li>Any Two Starters</li>                                       
                                        <li>Any Two Main Courses (Curries)</li>
                                        <li>Accompaniments <br>(Roti / Naan)</li>                                       
                                        <li>One Dessert</li>
                                        <li>Salad</li>  
                                        
                                        
                                    </ul>
                                     <div class="button-top text-center">
										<a href="#book" class="custom-button" style="background-color:#ffffff !important;color:#B11F28 !important;">Enquire of Catering</a>
									</div>
                                </div>
                            </div>
							
							<div class="col-lg-3">
                                <div class="opening-time  d-flex align-items-center">
                                    <h3 class="text-center" style="color:#ffffff;padding-bottom:8px">£18 Per Person (Non-Veg)</h3>
                                    <ul class="time-list">
                                        
										<li>Pappad/Chutney</li>                                      
                                        <li>Any Two Starters</li>                                       
                                        <li>Any Two Main Courses (Curries)</li>
                                        <li>Accompaniments <br>(Roti / Naan)</li>                                       
                                        <li>One Dessert</li>
                                        <li>Salad</li>  
                                        
                                        
                                    </ul>
                                     <div class="button-top text-center">
										<a href="#book" class="custom-button" style="background-color:#ffffff !important;color:#B11F28 !important;">Enquire of Catering</a>
									</div>
                                </div>
                            </div>
                        </div>
						
						
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	
	   <section class="menu-section" id="menu">
        <div class="container-fluid">
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="text-center">
                <h3 >Terms & Conditions</h3>
                </div>
                <div class="col-lg-12">
                    <div class="menu-secrion-innner">
					
                        <div class="row">                            
                            <div class="col-lg-12">
                                <div class="opening-time  d-flex align-items-start">
                                   <ul class="time-list">
						
                                        <li>1. You are free to choose any dishes from our <a href='takeaway.php?mtype=takeaway-menu' style='color:#ffffff'>regular menu</a> or from below.</li>                                       
                                        <li>2. Bespoke Menu available upon request.</li>
										<li>3. Please contact us for any  special requirement i.e. allergen / dieteray </li>
                                        <li>4. MInimum 50% advance payment on confirmation. In case of cancellation, whole amount is refunded provided cancellation happens 7 days in advance.</li>
                                        <li>5. Above price includes food only. All other services i.e. Serving staff, Crockery and Cutley would be charged extra.</li>
										<li>6. Minimum 30 Persons.</li>
										
                                    </ul>
                                    <div class="button-top text-center">
										<a href="#book" class="custom-button" style="background-color:#ffffff !important;color:#B11F28 !important;">Enquire of Catering</a>
									</div>                                    
                                </div>
                            </div>
                        </div>
						
						
                    </div>
                </div>
            </div>
        </div>
    </section>
	
    <!-- ==========Menu-Section========== -->
 <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="content">
                        <div class="section-header">
                            <h6 class="sub-title extra-padding wow fadeInUp">
                                ABOUT US
                            </h6>
                            <h2 class="title extra-padding wow fadeInUp">
                                Dedicated To 
                                Delight You     
                            </h2>
                            <p>
                                Dangal is the perfect destination in Edinburgh for all of your favourite Indian healthy temptations, it is a fun and vibrant space offering modern culinary styles, where food is made with health & nutrition in mind, for instance, we use much less oil in our cooking than a typical Indian Catering. Serving flavoursome Indian cuisines, Dangal takes pride in helping you experience Indian culture though food. We successfully elevate the culinary experience to the next level with our ‘Preserving Indian culture’ concept. Not only can you dine in two of our incredible venues, but we also deliver our incredible cuisine direct to your door.
                            </p>
                            <p>
                                A meal at Dangal is one you won't soon forget and value you won’t believe.
                            </p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="img">
                        
                        <img src="assets/images/Dangal_5.JPG" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <!-- ==========Statistics-Section========== -->
    <!-- ==========Statistics-Section========== -->


 
    <!-- ==========Event-Section========== -->

    <!-- ==========Amenities-Section========== -->
    <section class="amenities-section" id="book">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="content">
                        <div class="section-header">
                            <h6 class="sub-title wow fadeInUp">
                                Enquire Now
                            </h6>
                            <h2 class="title wow fadeInUp">
                                Make Plans To Book Us
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center" >
                <div class="col-lg-6 col-md-6 ">
                    <div class="single-amenities">
                       <form class="form" method="post" action="">
                       <?php
                            if(isset($_POST['submit'])){
                                $name = $_POST['name'];
                                $phoneNumber = $_POST['phoneNumber'];
								$email = $_POST['email'];
								
								$address = $_POST['address'];
								$city = $_POST['city'];
								
								$eventtype = $_POST['eventtype'];
								$guestcount = $_POST['guestcount'];

                                $planningon = $_POST['planningon'];								
                                $requirement = $_POST['requirement'];

                                global $con;
								$sql = "INSERT INTO `cateringenquiries` (`id`, `name`, `phoneNumber`, `email`,`address`,`city`,`eventtype`,`guestcount`,`datetime`, `planningon`,`requirement` ) VALUES (NULL, :name, :phoneNumber, :email, :address, :city, :eventtype, :guestcount, CURRENT_TIMESTAMP, :planningon, :requirement)";
								
								$stmt = $con->prepare($sql);
								
								$stmt->bindValue(':name', $name, PDO::PARAM_STR);
								$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);			
								$stmt->bindValue(':email', $email, PDO::PARAM_STR);
								
								$stmt->bindValue(':address', $address, PDO::PARAM_STR);
								$stmt->bindValue(':city', $city, PDO::PARAM_STR);
								
								$stmt->bindValue(':eventtype', $eventtype, PDO::PARAM_STR);
								$stmt->bindValue(':guestcount', $guestcount, PDO::PARAM_INT);
								$stmt->bindValue(':planningon', $planningon, PDO::PARAM_STR);

								$stmt->bindValue(':requirement', $requirement, PDO::PARAM_STR);

                                if($stmt->execute()){
									
									$lastInsertId = $con->lastInsertId();									
								
									$sql_history = "INSERT INTO `catering_enq_history` (`id`, `enquiryId`, `datetime`, `comments` ) VALUES (NULL, :enquiryId, CURRENT_TIMESTAMP,  :comments)";
									
									$stmt_history = $con->prepare($sql_history);
									
									$stmt_history->bindValue(':enquiryId', $lastInsertId, PDO::PARAM_INT);
									$stmt_history->bindValue(':comments', $requirement, PDO::PARAM_STR);
									$stmt_history->execute();
									
                                    echo "<script>alert('Thank you for submitting,will get back to you soon')</script>";
									//return gtag_report_conversion('https://www.dangal.co.uk/best-indian-catering-edinburgh/index.php');
									
                                } else {
									 echo "<script>alert('Message couldn't be sent successfully. Please contact 0131-334-2176')</script>";
								}
								
                            }

                       ?>                       
							<input type="text" class="form-group"  placeholder="Full Name *" name="name" required>			   
							<input type="text" class="form-group"  placeholder="Mobile Number *" name="phoneNumber" required>
							<input type="email" class="form-group"  placeholder="E-Mail" name="email">
							
							<input type="text" class="form-group"  placeholder="Address" name="address">
							<input type="text" class="form-group"  placeholder="City *" name="city" required>

							<input list="eventtype" class="form-group"  placeholder="Event Type *" name="eventtype" required>
								<datalist id="eventtype">
									<option value="Birthday" selected>
									<option value="Anniversary">
									<option value="Family Reunion">
									<option value="Baby Shower">
									<option value="Funeral">
									<option value="Wedding">
									<option value="Corporate Event">
									<option value="Other event">
								</datalist>

							<input type="number" class="form-group"  placeholder="Expected Guest Count *" name="guestcount" required>
							
							<label for="planningon">When are you planning on *</label>
							<input type="date" class="form-group"  name="planningon" required>	
							
							<textarea class="form-group"  placeholder="Additional Info" name="requirement"></textarea>
							<input type="submit" class="form-group btn-primary"  name="submit" >
							
                       
                       <form>
                    </div>
                </div>
               
               
            </div>
        </div>
    </section>
    <!-- ==========Amenities-Section========== -->

    <!-- ==========Newslater-Section========== -->
     <footer class="footer-section">
         <img class="shape" src="assets/images/footer-shape.png" alt="">
         <img class="f-left wow fadeInLeft"  data-wow-delay=".5s" src="assets/images/aimg1.png" alt="">
         <img class="f-right wow fadeInRight"  data-wow-delay=".5s" src="assets/images/aimg2.png" alt="">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="top-area">
                            <div class="logo">
                                <img src="assets/images/logo/dangallogo.png" width="200px" alt="logo">
                            </div>
                            <div  class="footer-social-links">
                                <span class="label">Follow us :</span>
                                <ul>
                                    <li>
                                        <a href="https://www.facebook.com/DangalCatering/">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/TheDangal">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.pinterest.co.uk/DangalCatering/">
                                            <i class="fab fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/DangalCatering/">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="newslater-wrapper">
                            
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="info-box">
                                    <div class="icon">
                                        <img src="assets/images/ii1.png" alt="">
                                    </div>
                                    <p>
                                        Phone Number:
                                    </p>
                                    <p>
                                       
<p>(0131) 334 2176
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="info-box">
                                    <div class="icon">
                                        <img src="assets/images/ii2.png" alt="">
                                    </div>
                                   
                                    <p>
                                        47-49 Duke Street, Edinburgh EH6 8HH
                                    </p>
                                </div>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <div class="info-box">
                                    <div class="icon">
                                        <img src="assets/images/ii2.png" alt="">
                                    </div>
                                    <p>
                                        215 St John's Rd, Edinburgh EH12 7UU
                                    </p>
                                    
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="info-box">
                                    <div class="icon">
                                        <img src="assets/images/ii3.png" alt="">
                                    </div>
                                    <p>
                                        Drop us a line:
                                    </p>
                                    <p>
                                        <a href="mailto:events@dangal.co.uk" style="color:#ffffff">events@dangal.co.uk</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            Copyright © 2021.All Rights Reserved By <a href="https://www.dangal.co.uk" style="color:#ffffff">Dangal</a>, Designed and Developed By <a href="https://www.easewebs.com" style="color:#ffffff">EaseWebs</a>
                        </div>
                    </div>
                </div>
            </div>
    </footer> 
    <!-- ==========Newslater-Section========== -->

    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/magnific-popup.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/odometer.min.js"></script>
    <script src="assets/js/viewport.jquery.js"></script>
    <script src="assets/js/nice-select.js"></script>
    <script src="assets/js/main.js"></script>
</body>


</html>